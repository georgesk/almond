#!/bin/sh

version=$2
orig_tgz=almond_${version}.orig.tar.gz
orig_txz=almond_${version}+dfsg.orig.tar.xz
newdir=$(echo $orig_tgz | sed -e 's/.orig.tar.gz//' -e 's/_/-/')+dfsg


wd=$(pwd)

cd ..

tar xzf $orig_tgz && mv almond-${version} ${newdir}

# remove the embedded library
rm -f ${newdir}/tests/plugins/text.js

if [ -L $orig_tgz ] ; then rm $(readlink $orig_tgz); fi
rm $orig_tgz

tar cJf $orig_txz $newdir
rm -rf $newdir

echo "Created ../$orig_txz"

